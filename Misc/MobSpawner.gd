extends Node2D

export (NodePath) var _tile_map_path : NodePath 
export(PackedScene) var _enemy_path

var delays := [9.0, 8.0, 7.0, 5.6, 4.0]
var spawn_number := [1, 2, 3, 3, 4]
var enemy_speeds := [1, 1.5, 1.8, 2.0, 2.4]
var spawn_delay := 0.9
var level := 0
var counter := 0
var counter_max := 8
var path_iterations := 26
var spawn_point: Vector2
var spawn_dir: float

onready var max_level = delays.size() - 1
onready var spawn_counter = spawn_number[level]
onready var _tile_map := get_node(_tile_map_path)
onready var _map_cell_size = _tile_map.cell_size

func _on_Start_timeout():
	_on_Wave_timeout()
	$Wave.start(delays[level])


func _on_Wave_timeout():
	counter += 1
	_set_spawn_point()
	$Spawn.start(spawn_delay)
	spawn_counter = spawn_number[level]
	if counter >= counter_max:
		counter = 0
		level = clamp(level + 1, 0, max_level)
		$Wave.wait_time = delays[level]


func _on_Spawn_timeout():
	if spawn_counter > 0:
		var dir = spawn_dir + 180 + rand_range(-3.0, 3.0)
		var dir_vec = Vector2.RIGHT.rotated(deg2rad(dir))
		var enem_instance = _enemy_path.instance()
		get_node("/root/Main/Enemies").add_child(enem_instance)
		enem_instance.position = spawn_point
		enem_instance.ini(dir_vec, enemy_speeds[level])
		spawn_counter -= 1
	else:
		$Spawn.stop()


func _set_spawn_point():
	var zone = _tile_map.get_used_rect()
	var center = (zone.position + (zone.size / 2).round()) * _map_cell_size
	var ofs = randf()
	var incr = 1.0 / path_iterations
	var order = []
	var points = {}
	for i in range(path_iterations):
		$Path/Pointer.unit_offset = ofs + (i * incr)
		if _tile_map.get_cellv(_tile_map.world_to_map($Path/Pointer.position)) == -1:
			$Path/Pointer.rotation = $Path/Pointer.position.angle_to_point(center)
			$Path/Pointer/Ray.force_raycast_update ( )
			if $Path/Pointer/Ray.is_colliding():
				var pd = $Path/Pointer.position.distance_to($Path/Pointer/Ray.get_collision_point())
				order.append(pd)
				points[pd] = $Path/Pointer.unit_offset
	order.sort()
	var chosen = (randi() % 10) + 1
	chosen = clamp(chosen, 1, order.size() - 1)
	$Path/Pointer.unit_offset = points[order[- chosen]]
	$Path/Pointer.rotation = $Path/Pointer.position.angle_to_point(center)
	spawn_dir = $Path/Pointer.rotation_degrees
	spawn_point = $Path/Pointer.position
