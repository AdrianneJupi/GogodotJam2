extends Node2D


var state = 0 #launch #wait #seek
var dest : Vector2
var spd := rand_range(2.5, 8.5)

onready var cursor_system = get_node("/root/Main/CursorSystem")
onready var cursor = get_node("/root/Main/CursorSystem/Cursor")


func ini(dir: int):
	$Timer.start(rand_range(10.0, 14.0))
	var dist = (randi() % 15 ) + 10
	dest = global_position + (Vector2(dist, 0).rotated(deg2rad(dir)))

func _process(delta):
	$Sprite.frame = 1 if $Sprite.frame == 0 else 0
	match state:
		0:
			position = position.linear_interpolate(dest, spd * delta)
			if position.distance_to(dest) <= 1:
				state = 1
				position = dest
		1:
			for area in $Area2D.get_overlapping_areas():
				if area.is_in_group("cursor"):
					state = 2
					spd = rand_range(6.0, 10.0)
		2:
			var dest = cursor.position + Vector2(20, -10)
			position = position.linear_interpolate(dest, spd * delta)
			if position.distance_to(dest) <= 4:
				cursor_system.energy += Global.pellet_value
				queue_free()


func _on_Timer_timeout():
	if state != 2:
		queue_free()
