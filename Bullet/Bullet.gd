extends Area2D


export (float) var speed: = 200

func _physics_process(delta: float) -> void:
	_move(delta)

func to(position: Vector2):
	self.position = position
	return self


func towards(direction: Vector2):
	self.transform.x = direction
	return self


func _move(delta) -> void:
	position += transform.x * speed * delta


func _on_Bullet_area_exited(area):
	if area.is_in_group("field"):
		queue_free()


func _on_Bullet_area_entered(area):
	if area.is_in_group("enemy"):
		area.get_parent().hit()
		Effect.emit_signal("screen_shake", 0.1, 2)
		queue_free()
