extends Node2D


const scan_update := 0.7

export(PackedScene) var small_bullet
export(PackedScene) var medium_bullet
export(PackedScene) var big_bullet

var direction := 0 setget direction_set
var size := "Medium"
var active := false
var firerate : float
var bullet_type

var has_target : bool
var target

var specs := {"Small": [0.9, 70],
			"Medium": [0.7, 74],
			"Big": [0.5, 78]}


func _process(delta):
	pass

func ini(size_set):
	active = true
	size = size_set
	firerate = specs[size_set][0]
	var rad = specs[size_set][1]
	match size_set:
		"Small": bullet_type = small_bullet
		"Medium": bullet_type = medium_bullet
		"Big": bullet_type = big_bullet
	$Range/Area2D/CollisionShape2D.shape.radius = rad
	$Range/Area2D.position.x = rad - 16
	$Range/Area2D.set_deferred("monitoring", true)
	$Range.rotation_degrees = direction
	call_deferred("scan")
	$Firerate.call_deferred("start", firerate)


func scan():
	var detected = $Range/Area2D.get_overlapping_areas()
	var targ = false
	if not detected.empty():
		var dist = 400
		for area in detected:
			if area.is_in_group("enemy"):
				var ds = position.distance_to(area.position)
				if ds < dist:
					dist = ds
					targ = area
	has_target = false if typeof(targ) == TYPE_BOOL else true
	if has_target:
		target = targ
	$Scan.start(scan_update)


func shoot():
	$Sprite.frame = 0
	$Sprite.play()
	Effect.emit_signal("screen_shake", 0.02, 1)
	self.direction = rad2deg(target.global_position.angle_to_point($Center/Muzzle.global_position))
	get_node("/root/Main/Bullets").add_child(
		bullet_type.instance().to($Center/Muzzle.global_position).\
		towards(Vector2.RIGHT.rotated(deg2rad(direction))))


func direction_set(dir: int):
	direction = dir
	_aim_gun_sprite(direction)


func _aim_gun_sprite(dir: int):
	var flip = Vector2(0, 0)
	var ang = dir % 360
	if ang < 0:
		ang += 360
	ang = round(float(ang) / 45.0) * 45
	match int(ang) % 90:
		0:
			$Sprite.set_animation("Gun" + size + String(0))
			$Sprite.rotation_degrees = ang
		45:
			$Sprite.rotation_degrees = 0
			$Sprite.set_animation("Gun" + size + String(45))
			match ang:
				135.0:
					flip = Vector2(1, 0)
				225.0:
					flip = Vector2(1, 1)
				315.0:
					flip = Vector2(0, 1)
	$Sprite.flip_h = flip.x
	$Sprite.flip_v = flip.y
	$Center.rotation_degrees = dir

func _on_Scan_timeout():
	scan()

func _on_Firerate_timeout():
	if has_target:
		if not is_instance_valid(target):
			scan()
			if has_target:
				shoot()
		else:
			shoot()
	if not active:
		$Firerate.stop()
