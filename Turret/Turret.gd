extends Node2D



func initialize(base_rot: int, base_sprite: String, river_rot: int,
		river_fh: bool, river_fv: bool, river_sprite: String, turret_pos: Vector2, 
		turret_rot: int, turret_size: String):
	$TurretBase.rotation_degrees = base_rot
	$TurretBase.play(base_sprite)
	$RiverCorner.rotation_degrees = river_rot
	$RiverCorner.flip_h = river_fh
	$RiverCorner.flip_v = river_fv
	$RiverCorner.play(river_sprite)
	$TurretGun.position = turret_pos
	$TurretGun.size = turret_size
	$TurretGun.direction = turret_rot
	$TurretGun.ini(turret_size)

func cleanup_gun():
	$TurretGun.queue_free()
