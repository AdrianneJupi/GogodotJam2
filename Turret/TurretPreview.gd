extends Node2D

export(PackedScene) var turret

var active = true
var main_active = true

var _river_flow : int
var _result_flow : int
var _possible_sizes := []
var _possible_directions := []
var _river_direction : Vector2

var _river_toggled := false
var _area_hovered := false

var _selected_size := 2

var can_build : bool

signal cursor_rotated(direction)
signal turret_spawned(instance, river_result_flow)

func _ready():
	visible = false


func _input(event):
	if main_active and active:
		if event.is_action_pressed("toggle_direction") or \
				(event.is_action_pressed("toggle_general") and not _area_hovered):
			_toggle_river_direction()
		elif event.is_action_pressed("switch_size") or \
				(event.is_action_pressed("toggle_general") and _area_hovered):
			_switch_turret_size(1)
		elif event.is_action_pressed("create_turret"):
			if can_build:
				_spawn_turret()


func show_preview(pos: Vector2, type, cur_dir, avail_dir):
	global_position = pos
	visible = true
	$RiverCorner.playing = true
	active = true
	_river_flow = type
	_get_possible_sizes(_river_flow)
	_selected_size = clamp(_selected_size, 0, _possible_sizes.size()-1)
	_possible_directions = avail_dir.duplicate()
	if _river_toggled:
		_possible_directions.invert()
	_river_direction = cur_dir
	_set_river_sprite(_river_direction, _possible_directions[0],
					_possible_sizes[_selected_size])


func hide_preview():
	visible = false
	$RiverCorner.playing = false
	active = false


func _spawn_turret():
	Effect.emit_signal("screen_shake", 0.2, 2)
	var new_turret = turret.instance()
	new_turret.global_position = global_position
	new_turret.initialize($TurretBase.rotation_degrees, $TurretBase.animation,
			$RiverCorner.rotation_degrees, $RiverCorner.flip_h, $RiverCorner.flip_v,
			$RiverCorner.animation, $TurretGun.position, $TurretGun.direction, 
			_possible_sizes[_selected_size])
	Effect.one_shot_anim(get_tree().root, "turret_wave", 
			$TurretGun.global_position, 22)
	emit_signal("turret_spawned", new_turret, _possible_directions[0], _result_flow)



func _toggle_river_direction():
	_possible_directions.invert()
	_river_toggled = not _river_toggled
	_set_river_sprite(_river_direction, _possible_directions[0],
					_possible_sizes[_selected_size])

func _switch_turret_size(amount: int):
	_selected_size += amount
	if _selected_size < 0:
		_selected_size = _possible_sizes.size()-1
	if _selected_size > _possible_sizes.size()-1:
		_selected_size = 0
	_set_river_sprite(_river_direction, _possible_directions[0],
					_possible_sizes[_selected_size])


func _set_turret_sprite(cur_dir: Vector2, riv_dir: Vector2, size: String):
	var ang = rad2deg(((riv_dir * -1) + cur_dir).angle())
	emit_signal("cursor_rotated", ang + 135)
	$TurretBase.rotation_degrees = ang - 135
	$TurretBase.play("Base" + String(size))
	$TurretGun.size = size
	$TurretGun.global_position = $TurretBase/TurretPosition.global_position
	var tur_ang = rad2deg($TurretBase.position.angle_to_point($TurretGun.position))
	#var tur_ang = rad2deg(cur_dir.angle()) #ALIGN WITH RIVER
	$TurretGun.direction = tur_ang + 180


func _set_river_sprite(from_dir: Vector2, to_dir: Vector2, size: String):
	_result_flow = _get_resulting_flow(_river_flow, size)
	_set_river_orientation(from_dir, to_dir)
	_set_river_preview(from_dir, to_dir, _result_flow)
	$RiverCorner.play("River" + String(_river_flow) + String(_result_flow))
	_set_turret_sprite(_possible_directions[0],
			_river_direction, _possible_sizes[_selected_size])


func _set_river_preview(from_dir: Vector2, to_dir: Vector2, future_flow: int):
	$RiverMask.rotation = from_dir.angle()
	$RiverPreview.rotation = to_dir.angle()
	$RiverPreview.frame = future_flow


func _get_possible_sizes(type: int):
	var sizes = ["Big", "Medium", "Small"]
	match type:
		0: sizes.pop_front()
		2: sizes.pop_back()
	_possible_sizes = sizes


func _get_resulting_flow(cur_flow: int, turret_type: String):
	match turret_type:
		"Big": cur_flow -= 1
		"Small": cur_flow += 1
	return clamp(cur_flow, 0, 2)


func _set_river_orientation(from_dir: Vector2, to_dir: Vector2):
	var dirs = [from_dir, to_dir]
	$RiverCorner.rotation_degrees = 0
	$RiverCorner.flip_h = false
	$RiverCorner.flip_v = false
	match dirs:
		[Vector2(-1, 0), Vector2(0, -1)]:
			$RiverCorner.rotation_degrees = 90
			$RiverCorner.flip_h = true
		[Vector2(0, 1), Vector2(-1, 0)]:
			$RiverCorner.flip_h = true
		[Vector2(1, 0), Vector2(0, -1)]:
			$RiverCorner.rotation_degrees = 90
			$RiverCorner.flip_h = true
			$RiverCorner.flip_v = true
		[Vector2(0, -1), Vector2(1, 0)]:
			$RiverCorner.flip_v = true
		[Vector2(-1, 0), Vector2(0, 1)]:
			$RiverCorner.rotation_degrees = 90
		[Vector2(0, -1), Vector2(-1, 0)]:
			$RiverCorner.flip_h = true
			$RiverCorner.flip_v = true
		[Vector2(1, 0), Vector2(0, 1)]:
			$RiverCorner.rotation_degrees = 90
			$RiverCorner.flip_v = true


func _on_ToggleArea_mouse_entered():
	_area_hovered = true


func _on_ToggleArea_mouse_exited():
	_area_hovered = false
