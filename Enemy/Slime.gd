extends Node2D

export(PackedScene) var death_full_effect
export(PackedScene) var death_empty_effect

var life = 6

var direction: Vector2
var step_dist := 25
var next_step: Vector2
var speed = 1.2
var damage := 1

var state := 0
var full := false

var drink_distance := 8
var hit_flash_time := 0.1

var active = true


# Called when the node enters the scene tree for the first time.
func _ready():
	ini(Vector2.RIGHT)

func ini(dir: Vector2, spd_mult:= 1):
	direction = dir
	_set_next_step(1)
	speed *= spd_mult

func _process(delta):
	match state:
		0:
			_move(delta, 1)
		2:
			_move(delta, -1)

func _move(delta, dir_mult := 1):
	global_position = global_position.linear_interpolate(next_step, speed * delta)
	if global_position.distance_to(next_step) <= 3:
		global_position = next_step
		_set_next_step(dir_mult)

func _set_next_step(dir_mult: int):
	next_step = global_position + ((step_dist * dir_mult) * direction)
	$Sprite.set_frame(0)
	var sp = "MoveEmpty" if dir_mult == 1 else "MoveFull"
	$Sprite.play(sp)



func _on_Sprite_animation_finished():
	if $Sprite.animation == "Drink":
		$Light.scale *= 1.5
		$Sprite.position = Vector2.ZERO
		full = true
		$Sprite.play("MoveFull")
		_set_next_step(-1)
		state = 2


func _killed():
	var pls = Global.slime_loot_empty
	var shake = [0.15, 7]
	var anim = "slime_death1"
	if full:
		pls = Global.slime_loot_full
		shake = [0.25, 10]
		anim = "slime_death2"
		var ei2 = death_full_effect.instance()
		get_tree().root.add_child(ei2)
		ei2.position = global_position
	var ei = death_empty_effect.instance()
	get_tree().root.add_child(ei)
	ei.position = global_position
	Effect.one_shot_anim(get_tree().root, anim, global_position)
	Global.call_deferred("create_pellets", position, pls)
	$Hitbox.disconnect("area_exited", self, "_on_Hitbox_area_exited")
	Effect.emit_signal("screen_shake", shake[0], shake[1])
	Effect.screen_freeze(40)
	queue_free()


func hit():
	life -= 1
	$Sprite.material.set_shader_param("active", true)
	$HitFlash.start(hit_flash_time)
	if life <= 0:
		_killed()

func _on_Hitbox_area_exited(area):
	if area.is_in_group("field"):
		if full and active:
			Effect.one_shot_anim(get_tree().root, "life_lost", global_position, 20)
			Global.emit_signal("lose_life", damage)
		queue_free()



func _on_Proximity_body_entered(body):
	if state == 0:
		state = 1
		$Sprite.position += direction * drink_distance
		$Sprite.play("Drink")


func _on_Hitbox_body_entered(body):
	_killed()


func _on_HitFlash_timeout():
	$Sprite.material.set_shader_param("active", false)
