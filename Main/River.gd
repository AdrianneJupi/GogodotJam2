extends Node2D


export (NodePath) var _tile_map_path : NodePath 
export (Vector2) var _tile_map_size := Vector2(27, 15)

onready var _tile_map := get_node(_tile_map_path)
onready var _map_cell_size = _tile_map.cell_size

var available_cells_direction : Vector2
var available_cells_type : int
var available_cells : Dictionary

var river_cells := []

var _river_path := [
	[Vector2(13, -1), 1],
	[Vector2(13, 16), 2],
	]

func _ready():
	_update_river(_river_path)
	#_expand_river_path(Vector2(14, 10), Vector2(-1, 0), 1)


func _process(delta):
	pass

func _update_river(path: Array):
	_tile_map.clear()
	var rpath = _get_river_cells(path)
	_spawn_river_tiles(rpath)
	river_cells = _tile_map.get_used_cells()
	_get_available_cells(_river_path)


func _get_river_cells(path: Array) -> Array:
	var river_cells := []
	for cur_point in path:
		if cur_point == path.back():
			river_cells.append(cur_point)
			break
		var point = cur_point[0]
		var point_index = path.find(cur_point)
		var next_point = path[point_index + 1][0]
		var direction = (next_point - point).normalized()
		while point != next_point:
			river_cells.append([point, cur_point[1]])
			point += direction
	return river_cells


func _spawn_river_tiles(cells: Array) -> void:
	for cell in cells:
		var index = cells.find(cell)
		var prec = false if cell == cells.front() else cells[index - 1][0]
		var next = false if cell == cells.back() else cells[index + 1][0]
		var sprite = _get_tile_sprite(cell[0], prec, next, cell[1])
		_tile_map.set_cell(cell[0].x, cell[0].y, 0,
				 sprite[1], sprite[2], sprite[3], Vector2(sprite[0], 0))



func _get_tile_sprite(cur : Vector2, prec, next, type := 1) -> Array:
	var first = Vector2.ZERO if not prec else (cur - prec).normalized()
	var second = Vector2.ZERO if not next else (next - cur).normalized()
	var final = (first + second).normalized()
	match final:
		Vector2(1, 0): #STRAIGHTS
			return [type, false, false, true]
		Vector2(-1, 0):
			return [type, true, false, true]
		Vector2(0, 1):
			return [type, false, false, false]
		Vector2(0, -1):
			return [type, false, true, false]
		_:
			return [3, false, false, false]


func _get_border(pos: Vector2, dir: Vector2):
	match dir:
		Vector2(1, 0):
			return Vector2(_tile_map_size.x + 1, pos.y)
		Vector2(-1, 0):
			return Vector2(-1, pos.y)
		Vector2(0, 1):
			return Vector2(pos.x, _tile_map_size.y + 1)
		Vector2(0, -1):
			return Vector2(pos.x, -1)


func _check_direction(pos: Vector2, dir: Vector2) -> bool:
	var cnt = 0
	while (pos.x > 0 and pos.x <_tile_map_size.x) and \
			(pos.y > 0 and pos.y < _tile_map_size.y):
		pos += dir
		cnt += 1
		if _tile_map.get_cellv(pos) != -1:
			return false
	return false if cnt <= 1 else true


func _expand_river_path(pos: Vector2, dir: Vector2, type := 1):
	var old = _river_path.duplicate()
	_river_path.pop_back()
	_river_path.append([pos, type])
	_river_path.append([_get_border(pos, dir), type])
	_change_effect(_river_path, old, "river_create")
	_change_effect(old, _river_path, "river_destroy")
	_update_river(_river_path)


func delete_point(pindex: int):
	var point = _river_path[pindex + 1]
	var prev = _river_path[pindex]
	var dir = (point[0] - prev[0]).normalized()
	var pos = prev[0]
	var flow = prev[1]
	var new_path = []
	for p in _river_path:
		if _river_path.find(p) <= (pindex):
			new_path.append(p)
	_change_effect(_river_path, new_path, "river_destroy")
	_river_path = new_path.duplicate()
	_expand_river_path(pos, dir, flow)
	_update_river(_river_path)


func _get_available_cells(path: Array):
	var segment = _get_river_cells([path[-2], path[-1]])
	segment.pop_back()
	segment.pop_front()

	var final = {}
	var directions = [Vector2.UP, Vector2.DOWN, Vector2.RIGHT, Vector2.LEFT]
	directions.erase((segment[1][0] - segment[0][0]).normalized())
	directions.erase((segment[0][0] - segment[1][0]).normalized())

	for cell in segment:
		var pos = cell[0]
		var available_dirs = []
		for dir in directions:
			if _check_direction(pos, dir):
				available_dirs.append(dir)
		if not available_dirs.empty():
			final[pos] = available_dirs
	
	available_cells_type = segment[0][1]
	available_cells_direction = (segment[1][0] - segment[0][0]).normalized()
	available_cells = final


func _change_effect(first_river: Array, second_river: Array, effect_name: String):
	var first_cells = _get_river_cells(first_river)
	var second_cells = _get_river_cells(second_river)
	var targs = []
	for el in first_cells:
		if not el in second_cells:
			targs.append(el)
	for tar in targs:
		Effect.one_shot_anim(get_tree().root, effect_name, (tar[0] * _map_cell_size) + Vector2(8, 8), 25)

