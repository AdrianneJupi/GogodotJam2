extends Node2D

export (NodePath) var _tile_map_path : NodePath 
export (Vector2) var _cursor_area_start := Vector2(1, 1)
export (Vector2) var _cursor_area_end := Vector2(26, 14)
export (String) var _turrets_path := "/root/Main/Turrets"
export (String) var _enemies_path := "/root/Main/Enemies"

onready var _tile_map := get_node(_tile_map_path)
onready var _map_cell_size = _tile_map.cell_size

var energy := 125 setget energy_set
var life := 4 setget life_set

var active := true

var _cursor_position : Vector2 
var _cursor_catchup := false
var _cursor_animation : String
var on_turret

var tutorial_offset = Vector2(54, 0)
var tutorial_text := ["Build: Left CLick", "Rotate: Press X", "Switch: Press C"]
var tutorial_rot = true
var tutorial_active = true
var info_energy = true
var message_buy = true
var delete_text = "Destroy: Right Click"

func _ready():
	randomize()
	Global.connect("lose_life", self, "on_lose_life")

func _process(delta):
	if active:
		_update_cursor_position()
		_cursor_check_catchup(delta)
#	$Label.text = String(Performance.get_monitor(Performance.TIME_FPS))


func _input(event):
	if event.is_action_pressed("destroy_turret"):
		if active:
			if (not _cursor_catchup) and (typeof(on_turret) != TYPE_BOOL):
				Effect.emit_signal("screen_shake", 0.4, 2)
				var pos = _tile_map.world_to_map(on_turret.position)
				var turrets = get_node(_turrets_path).get_children()
				var index = turrets.find(on_turret)
				for tur in turrets:
					if turrets.find(tur) >= index:
						Effect.one_shot_anim(get_tree().root, "turret_destroy", 
							tur.get_node("TurretGun").global_position, 36)
						Effect.one_shot_anim(get_tree().root, "turret_wave", 
							tur.get_node("TurretGun").global_position, 22)
						get_node(_turrets_path).remove_child(tur)
						tur.queue_free()
						Global.create_pellets(tur.position + Vector2(8, 8), Global.cost_return)
				$River.delete_point(index)
				_get_cell_type(pos)


func _update_cursor_position():
	var cur_pos = _tile_map.world_to_map(_cursor_position)
	var mouse_pos = _tile_map.world_to_map(get_global_mouse_position())
	mouse_pos.x = clamp(mouse_pos.x, 1, $River._tile_map_size.x - 1)
	mouse_pos.y = clamp(mouse_pos.y, 1, $River._tile_map_size.y - 1)
	if cur_pos != mouse_pos:
		_cursor_position = mouse_pos * _map_cell_size
		_cursor_catchup = true
		_get_cell_type(mouse_pos)
		$Cursor/Sprite.play(_cursor_animation)


func _get_cell_type(position: Vector2):
	on_turret = _find_turret(position)
	if typeof(on_turret) != TYPE_BOOL:
		_cursor_animation = "cursor_on"
		$Cursor/Sprite.modulate = Color.white
		$TurretPreview.hide_preview()
		if tutorial_active:
			$Cursor/Tutorial.visible = true
			$Cursor/Tutorial.update_text(delete_text)
			tutorial_rot = false
			_tutorial_side()
		_update_hud_info(true)
		$Message.visible = false
		message_buy = false
		return
	if position in $River.available_cells:
		_cursor_animation = "cursor_corner"
		var av_dir = $River.available_cells.get(position)
		var riv_dir = $River.available_cells_direction
		var riv_type = $River.available_cells_type
		$TurretPreview.show_preview(_cursor_position, riv_type, riv_dir, av_dir)
		if tutorial_active:
			$Cursor/Tutorial.visible = true
			$Cursor/Tutorial.update_text(tutorial_text[0])
			tutorial_rot = true
			_tutorial_side()
		_update_hud_info(true)
		$Message.visible = true
		message_buy = true
		_update_hud_message(can_buy_turret())
		return
	if position in $River.river_cells:
		_cursor_animation = "cursor_off"
		$TurretPreview.hide_preview()
		$Cursor/Tutorial.visible = false
		message_buy = false
		$Cursor/Sprite.modulate = Color.white
		$Message.visible = true
		$Message.update_text("Turrets can only be built downstream")
		_update_hud_info(false)
		return
	else:
		_cursor_animation = "cursor_off"
		$TurretPreview.hide_preview()
		$Cursor/Tutorial.visible = false
		_update_hud_info(true)
		message_buy = false
		$Cursor/Sprite.modulate = Color.white
		$Message.visible = true
		$Message.update_text("Cannot build turret on land")


func _cursor_check_catchup(delta):
	if _cursor_catchup:
		var spd = 24.0 * delta
		$Cursor.position = $Cursor.position.linear_interpolate(
				_cursor_position, spd)
		if $Cursor.position.distance_to(_cursor_position) < 1.0:
			$Cursor.position = _cursor_position
			_cursor_catchup = false
			#update cursor text


func _tutorial_side():
	var tx = tutorial_offset.x
	var pos = _tile_map.world_to_map(_cursor_position)
	var fx = tx + 16 if pos.x <= 13 else -tx
	$Cursor/Tutorial.position = Vector2(fx, 0)


func _update_hud_info(enr: bool):
	if enr: 
		$Cursor/Infos.update_text("CUR: " + String(energy))
	else:
		$Cursor/Infos.update_text("LIFE: " + String(life))
	info_energy = enr

func _update_hud_message(can_buy: bool):
	if can_buy:
		$Cursor/Sprite.modulate = Color.white
		$Message.update_text("Turret cost: " + String(Global.turret_cost) \
					+ " CURRENCY, Current CURRENCY: " + String(energy))
	else:
		$Cursor/Sprite.modulate = Color(0.5, 0.5, 0.5)
		$Message.update_text("Not enough CURRENCY to build turret")


func life_set(lf):
	life = lf
	if not info_energy:
		_update_hud_info(false)

func energy_set(en):
	energy = en
	if info_energy:
		_update_hud_info(true)
	if message_buy:
		_update_hud_message(can_buy_turret())

func can_buy_turret():
	var res = (energy - Global.turret_cost) >= 0
	$TurretPreview.can_build = res
	return res



func _on_TurretPreview_cursor_rotated(direction):
	$Cursor/Sprite.rotation_degrees = direction


func _find_turret(position: Vector2):
	for turret in get_node(_turrets_path).get_children():
		if _tile_map.world_to_map(turret.position) == position:
			return turret
	return false


func _on_turret_spawned(instance, direction, river_result_flow):
	self.energy -= Global.turret_cost
	get_node(_turrets_path).add_child(instance)
	var tur_pos = _tile_map.world_to_map(instance.position)
	_get_cell_type(tur_pos)
	if get_node(_turrets_path).get_child_count() >= 2:
		tutorial_active = false
		$Cursor/Tutorial.visible = false
	$River._expand_river_path(tur_pos, direction, river_result_flow)



func _on_Tutorial_timeout():
	tutorial_text.append(tutorial_text[0])
	tutorial_text.pop_front()
	if tutorial_rot and tutorial_active:
		$Cursor/Tutorial.update_text(tutorial_text[0])


func on_lose_life(amount):
	Effect.emit_signal("screen_shake", 0.5, 8)
	Effect.screen_freeze(70)
	self.life = clamp(life - amount, 0, 100)
	if life == 0 and active:
		Effect.emit_signal("screen_shake", 0.82, 9)
		active = false
		visible = false
		$TurretPreview.main_active = false
		for tur in get_node(_turrets_path).get_children():
			tur.cleanup_gun()
			Effect.one_shot_anim(get_tree().root, "turret_destroy", 
					tur.get_node("TurretGun").global_position, 36)
			Effect.one_shot_anim(get_tree().root, "turret_wave", 
					tur.get_node("TurretGun").global_position, 22)
		for enem in get_node(_enemies_path).get_children():
			enem.active = false
		$Cursor/Absorber.set_deferred("monitorable", false)
		$GameOver.start()




func _on_GameOver_timeout():
	get_tree().reload_current_scene()


