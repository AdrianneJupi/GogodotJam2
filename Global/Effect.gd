extends Node


const OneShotEffect: = preload("res://Effect/OneShotEffect.tscn")
signal screen_shake(duration, strength)


func _ready() -> void:
	randomize()


static func screen_freeze(duration: int) -> void:
	OS.delay_msec(duration)


func one_shot_anim(parent: Node, animation: String, offset: = Vector2.ZERO, depth:= 70) -> void:
	var target: = OneShotEffect.instance()
	target.position = offset
	target.z_index = depth
	#parent.add_child(target)
	parent.call_deferred("add_child", target)
	target.ini()
	target.play(animation)
	
