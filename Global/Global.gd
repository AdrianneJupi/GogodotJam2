extends Node

signal lose_life(value)

const turret_cost = 50
const cost_return = 32
const pellet_value = 2
const slime_loot_empty = 12
const slime_loot_full = 20

const pellet: = preload("res://Misc/Pellet.tscn")

func create_pellets(pos: Vector2, total_value: int):
	var nb = floor(total_value / pellet_value)
	var cont = get_node("/root/Main/Pellets")
	var start = (randi() % 360)
	var spacing = 360 / nb
	for i in nb:
		start += spacing
		var pellet_instance = pellet.instance()
		cont.add_child(pellet_instance)
#		cont.call_deferred("add_child", pellet_instance)
		pellet_instance.global_position = pos
		pellet_instance.ini(start)
