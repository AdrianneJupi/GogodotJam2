extends AnimatedSprite



func ini():
	if animation in ["river_create", "river_destroy"]:
		speed_scale = 1.0 + (randf() * 0.1)

func _on_OneShotEffect_animation_finished():
	queue_free()

